# Fabric Common Product

> NOTE: The first draft of this document is scheduled for completion by 2020/01/08

> FIXME: Improve this overview

The Common API Orchestration (CAPIO) enables Fabric's modularity value
proposition. If an application needs product data, for example, it calles
CAPIO's product endpoints. CAPIO will then decide whether that product
information is found in Fabric's internal PIM or an external PIM.

This repository contains CAPIO's product microservice.

#### OpenAPI 3.0 Specifications

> NOTE: These are not yet live

| Environment |                                                                |
| ---         | ---                                                            | ---
| DEV         | [HTML](https://dev01.copilot.fabric.inc/common-product/docs)   | [JSON](https://dev01.copilot.fabric.inc/common-product/docs/json)   |
| QA          | [HTML](https://uat01.copilot.fabric.inc/common-product/docs)   | [JSON](https://uat01.copilot.fabric.inc/common-product/docs/json)   |
| STAGE       | [HTML](https://stage01.copilot.fabric.inc/common-product/docs) | [JSON](https://stage01.copilot.fabric.inc/common-product/docs/json) |
| SANDBOX     | [HTML](https://sandbox.copilot.fabric.inc/common-product/docs) | [JSON](https://sandbox.copilot.fabric.inc/common-product/docs/json) |
| PROD        | [HTML](https://prod01.copilot.fabric.inc/common-product/docs)  | [JSON](https://prod01.copilot.fabric.inc/common-product/docs/json)  |
| DEMO        | [HTML](https://demo.copilot.fabric.inc/common-product/docs)    | [JSON](https://demo.copilot.fabric.inc/common-product/docs/json)    |

#### Environment, Infra & Design

- [Data Model](./docs/data-model.md)
- [Systems Context](./docs/architecture.md)
- [Call Sequences](./docs/sequences.md)
