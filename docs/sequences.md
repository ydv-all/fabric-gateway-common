# Sequences

This document contains sequence diagrams for the various process flows involving
the Common API Orchestrator.

## Product Information

![Product Sequence](./capio-product-sequence.png)

## Payment Submission

![Payment Sequence](./capio-payment-sequence.png)

