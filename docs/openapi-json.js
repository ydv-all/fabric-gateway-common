const json = require('./openapi-all.yaml')

exports.handler = (event, context, callback) => {
  const response = {
    statusCode: 200,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(json)
  }

  callback(null, response)
}
