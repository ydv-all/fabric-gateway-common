# Data Model

## Common Product API

**Tables:**

    Name         | Key           | Value
    -            | -             | -
    Product      | <product-sku> | <product>
    ProductAttrs | <product-sku> | [<attribute>]

**Entities:**

    <product>

        {
            sku: <string>,
            parent: <product>,
            children: [<product-child>],
            bundle: [<product-child>],
            categories: [<category>],
            attributes: [<attribute>],
        }

    <product-child>

        {
            sku: <string>,
            attributes: [<attribute>],
        }

    <category>

        {
            id: <string>,
            breadcrumbs: [{
                id: <string>,
                attributes: [<attribute>],
            }],
            attributes: [<attribute>],
        }

    <attribute>

        {
            name: <string>,
            description: <string>,
            mapping: <string>,
            type: <enum [ TEXT, BOOLEAN, SERIAL, DECIMAL, INTEGER, DATETIME ]>,
            value: <variable>,
            children: [<attribute>]
        }

## Common Category API

**Tables:**

    Name         | Key           | Value
    -            | -             | -
    Category     | <category-id> | <category-single>
    CategoryTree | <category-id> | <category-tree>
    CategorySku  | <category-id> | [<product-sku>]

**Entities:**

    <category-single>

        {
            id: <string>,
            children: [<category-child>],
            breadcrumbs: [{
                id: <string>,
                attributes: {
                    mapped: {
                        [<mapping>: <attribute>],
                    },
                },
            }],
            attributes: {
                mapped: {
                    [<mapping>: <attribute>],
                },
                unmapped: [<attribute>]
            },
        }

    <category-child>

        {
            id: <string>,
            attributes: [<attribute>],
        }

    <category-tree>

        {
            id: <string>,
            children: [<category-tree>],
            attributes: [<attribute>],
        }

    <attribute>

        {
            name: <string>,
            description: <string>,
            mapping: <string>,
            type: <enum [ TEXT, BOOLEAN, SERIAL, DECIMAL, INTEGER, DATETIME ]>,
            value: <variable>
            children: [<attribute>]
        }

## Gaps & Open Questions

- We are missing channel
- We may need a <channel><sku>:<product> index
- How do we handle search? Is it a separate search DB or in Redis?
- How do we handle the re-cache
- We may need sub-document updates when consuming changes from queue

- Do we need to include `numSkus` and `numChildren`?
- We might need custom sets of attribute to limit the amount of data returned
  for specific use-cases: ecomm, supply chain (only wants 10 attributes that
  drive warehouse/order fulfillment)
    - Perhaps allow the customer to create their own attribute mappings? Maybe
      even groups of mappings?
- These documents might get big!
- Can we reduce the size? (msgpack, gzip, etc)
    - Not everyone needs all attrs all the time
