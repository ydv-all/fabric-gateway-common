const { API_NAME, SPLIT_STACKS_EXCLUSIONS, SLS_DEBUG } = process.env
if (SLS_DEBUG === '*') process.env['LOG_LEVEL'] = 'info'

const Logger = require('@teamfabric/logger')
const log = Logger('@serverless/stacks-map')
log.debug({ API_NAME, SPLIT_STACKS_EXCLUSIONS, SLS_DEBUG })

/**
 * Expects the following environment variables to be set on the build machine:
 * - API_NAME
 *   It iss the same stuff that is passed on as apiName option to serverless
 * - SPLIT_STACKS_EXCLUSIONS
 *   A comma separated list of API names to be excluded from custom migration
 */
module.exports = (resource, logicalId) => {
  let exclusions =
    process.env.SPLIT_STACKS_EXCLUSIONS &&
    process.env.SPLIT_STACKS_EXCLUSIONS.split(',')
  if (exclusions && exclusions.includes(process.env.API_NAME)) {
    log.debug('API_NAME is Excluded. Doing nothing')
    return
  }
  log.debug('Default Migration Only')
  return getDefaultMigration(logicalId)
}

const getDefaultMigration = logicalId => {
  if (
    logicalId.startsWith('ServerlessDeploymentBucket') ||
    logicalId.startsWith('ApiGatewayResource') ||
    logicalId.startsWith('ApiGatewayRestApi') ||
    logicalId.startsWith('ApiGatewayDeployment') ||
    logicalId.startsWith('ApiGatewayApiKey') ||
    logicalId.startsWith('ApiGatewayUsagePlan') ||
    logicalId.startsWith('IamRoleLambdaExecution') ||
    logicalId.startsWith('AuthorizerFunc')
  ) {
    log.info(`Migrating resource ${logicalId} to root stack.`)
    // Returning false tells the plugin to keep the resource in root stack
    return false
  }
}
