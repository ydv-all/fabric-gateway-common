/**
 * How to use:
 * 1. npm install
 * 2. Start local redis server with default port 6379
 * 3. run the script, modify configs as needed. Args self-explanatory, see line #15
 *   $ REDIS_SERVER_URL=redis://localhost:6379 node setupLocalDiscovery -a local -s local -p copilot -t 525600
 *
 */

// TODO: This is a POC script. To be improved
const {
  setCacheValue,
  getCacheKey
} = require('@teamfabric/discovery/app/lib/cache')

const addServicesToDiscovery = async () => {
  try {
    const { a: account, s: stage, p: platform, t: ttl } = require('minimist')(
      process.argv.slice(2)
    )
    const services = ['pim'] // add more
    const configs = await getServiceConfigs({
      account,
      stage,
      platform,
      services
    })
    for (serviceName of services) {
      const key = `${getCacheKey({
        account,
        stage,
        platform,
        serviceName
      })}_SDK`
      const config = configs[serviceName]
      console.debug({ serviceName, key, ttl, config })
      await setCacheValue({
        key,
        value: config,
        ttl: ttl || 525600 // Default to 1 year
      })
    }
  } catch (e) {
    console.error('Error', e)
    process.exit(1)
    // TODO: Take care of redis connection
  } finally {
    console.log('Exit')
    process.exit()
  }
}

// Abstracting this from the main logic
const getServiceConfigs = async ({ account, stage, platform, services }) => {
  let serviceConfigs = {}
  services.forEach(service => {
    let config = configs.filter(
      el =>
        el.account === account &&
        el.stage === stage &&
        el.platform === platform &&
        el.serviceName === service
    )
    serviceConfigs[service] = config && config[0]
  })
  return serviceConfigs
}

const configs = [
  // TODO: dummy values, add more and/or externalize
  {
    account: 'local', // TODO Might be validated for being an ObjectId downstream
    stage: 'local',
    platform: 'copilot',
    serviceName: 'pim',
    connections: {
      businessApi: {
        apiUrl: 'localhost:6010/api-pim',
        apiKey: 'd41d8cd98f00b204e9800998ecf8427e'
      },
      dataApi: {
        apiUrl: 'localhost:5010/data-pim',
        apiKey: 'd41d8cd98f00b204e9800998ecf8427e'
      },
      primaryDb: {
        serverType: 'MONGO_ATLAS',
        connectionString: 'mongodb://localhost:27017/local_pim'
      }
    }
  }
]

addServicesToDiscovery()
